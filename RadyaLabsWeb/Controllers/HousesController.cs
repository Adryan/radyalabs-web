﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RadyaLabsWeb.Models;

namespace RadyaLabsWeb.Controllers
{
    public class HousesController : Controller
    {
        private readonly RadyaLabsWebContext _context;

        public HousesController(RadyaLabsWebContext context)
        {
            _context = context;
        }

        // GET: Houses
        public async Task<IActionResult> Maps()
        {
            var houses = await _context.House.ToListAsync();
            string locations = "[";

            foreach(var item in houses)
            {
                locations += "{";
                locations += string.Format("'type': '{0}',", item.Type);
                locations += string.Format("'address': '{0}',", item.Adress);
                locations += string.Format("'lat': '{0}',", item.Latitude);
                locations += string.Format("'lng': '{0}',", item.Longitude);
                locations += "},";
            }

            locations += "]";

            ViewBag.Locations = locations;

            return View();
        }

        // GET: Houses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var house = await _context.House
                .FirstOrDefaultAsync(m => m.ID == id);
            if (house == null)
            {
                return NotFound();
            }

            return View(house);
        }

        // GET: Houses/Create
        public IActionResult Create()
        {
            List<SelectListItem> type = new List<SelectListItem>();
            type.Add(new SelectListItem { Text = "36", Value = "36" });
            type.Add(new SelectListItem { Text = "45", Value = "45" });
            type.Add(new SelectListItem { Text = "72", Value = "72" });

            ViewBag.HouseType = type;

            return View();
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.House.ToListAsync());
        }

        // POST: Houses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Type,Adress,Latitude,Longitude")] House house)
        {
            if (ModelState.IsValid)
            {
                _context.Add(house);    
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(house);
        }

        // GET: Houses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var house = await _context.House.FindAsync(id);
            if (house == null)
            {
                return NotFound();
            }

            List<SelectListItem> type = new List<SelectListItem>();
            type.Add(new SelectListItem { Text = "36", Value = "36" });
            type.Add(new SelectListItem { Text = "45", Value = "45" });
            type.Add(new SelectListItem { Text = "72", Value = "72" });

            ViewBag.HouseType = type;

            return View(house);
        }

        // POST: Houses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Type,Adress,Latitude,Longitude")] House house)
        {
            if (id != house.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(house);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HouseExists(house.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(house);
        }

        // GET: Houses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var house = await _context.House
                .FirstOrDefaultAsync(m => m.ID == id);
            if (house == null)
            {
                return NotFound();
            }

            return View(house);
        }

        // POST: Houses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var house = await _context.House.FindAsync(id);
            _context.House.Remove(house);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HouseExists(int id)
        {
            return _context.House.Any(e => e.ID == id);
        }
    }
}
