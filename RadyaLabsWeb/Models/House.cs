﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RadyaLabsWeb.Models
{
    public class House
    {
        [Required]
        public int ID { get; set; }
        public string Type { get; set; }
        public string Adress { get; set; }
        [Required]
        public string Latitude { get; set; }
        [Required]
        public string Longitude { get; set; }
    }
}
