﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace RadyaLabsWeb.Models
{
    public class RadyaLabsWebContext : DbContext
    {
        public RadyaLabsWebContext (DbContextOptions<RadyaLabsWebContext> options)
            : base(options)
        {
        }

        public DbSet<RadyaLabsWeb.Models.House> House { get; set; }
    }
}
